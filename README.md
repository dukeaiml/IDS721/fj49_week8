# Rust CLI Project

This Rust CLI project is a simple command-line tool for reading and searching through a CSV file containing information about people.

## Features

- **Read CSV Data**: The program reads data from a CSV file containing information about people, including their name, age, and location.
- **Search by Name**: Users can search for a person by their name via the command line.
- **Case Sensitivity**: The search functionality is case-sensitive, ensuring accurate results.

## Usage

To use the program, follow these steps:

1. **Clone the Repository**: Clone this repository to your local machine.

2. **Install Rust**: Ensure you have Rust and Cargo installed on your system. If not, you can install them from [here](https://www.rust-lang.org/tools/install).

3. **Navigate to the Project Directory**: Open a terminal and navigate to the directory where you cloned the repository.

4. **Build the Project**: Run `cargo build` to build the project.

5. **Run the Program**: Run the program with the path to your CSV file and the name you want to search. For example:
    ```bash
    ./target/debug/program_name "John"
    ```

6. **View Results**: The program will display information about the person if found, otherwise, it will print "Person not found".

## Screenshots ##

### Person found:

![Screenshot 1](screenshots/found.png)

### Person not found:

![2](screenshots/not_found.png)

### Tests passing:

![3](screenshots/tests.png)

## File Structure

- **src/**: Contains the source code files and the tests.
- **data/**: Contains the CSV file with people data.

## Dependencies

- **csv**: A CSV parsing library for Rust.

## Contributing

Contributions are welcome! If you find any bugs or want to suggest improvements, please open an issue or create a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
