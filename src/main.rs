use std::error::Error;
use std::fs::File;
use csv::ReaderBuilder;

// Define a struct to represent a person
#[derive(Debug, PartialEq)]
struct Person {
    name: String,
    age: u32,
    location: String,
}

// impl Person {
//     // Function to create a new person from CSV record
//     fn new(record: csv::StringRecord) -> Person {
//         Person {
//             name: record.get(1).unwrap_or("").to_string(),
//             age: record.get(2).unwrap_or("0").parse().unwrap_or(0),
//             location: record.get(3).unwrap_or("").to_string(),
//         }
//     }
// }

impl Person {
    // Function to create a new person from CSV record
    fn new(record: csv::StringRecord) -> Person {
        Person {
            name: record.get(0).unwrap_or("").to_string(), // Use index 0 for the name field
            age: record.get(1).unwrap_or("0").parse().unwrap_or(0), // Use index 1 for the age field
            location: record.get(2).unwrap_or("").to_string(), // Use index 2 for the location field
        }
    }
}


// Function to read people from CSV and return vector of people
fn read_people_from_csv(file_path: &str) -> Result<Vec<Person>, Box<dyn Error>> {
    let mut people = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    for result in rdr.records() {
        let record = result?;
        let person = Person::new(record);
        people.push(person);
    }
    Ok(people)
}

// // Function to find person by name (case-sensitive)
fn find_person_by_name<'a>(people: &'a [Person], name: &str) -> Option<&'a Person> {
    people.iter().find(|p| p.name == name)
}
// fn find_person_by_name<'a>(people: &'a [Person], person_name: &str) -> Option<&'a Person> {
//     people.iter().find(|person| person.name.to_lowercase() == person_name.to_lowercase())
// }

fn main() {
    // Read people from CSV
    let file_path = "/Users/farazjawed/Desktop/fj49_week8_rust_cli/Data/people.csv";
    let people = match read_people_from_csv(file_path) {
        Ok(ppl) => ppl,
        Err(err) => {
            eprintln!("Error reading people: {}", err);
            return;
        }
    };

    // Print out the people data to verify if it's being read correctly
    println!("People data:");
    for person in &people {
        println!("{:?}", person);
    }

    // Get person's name from command line argument
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <person_name>", args[0]);
        return;
    }
    let person_name = &args[1];

    println!("Searching for person with name: {}", person_name);

    // Find person by name and print information
    match find_person_by_name(&people, person_name) {
        Some(person) => println!("{:?}", person),
        None => println!("Person not found"),
    }
}



// fn main() {
//     // Read people from CSV
//     let people = match read_people_from_csv("/Users/farazjawed/Desktop/fj49_week9_rust_cli/Data/people.csv") {
//         Ok(ppl) => ppl,
//         Err(err) => {
//             eprintln!("Error reading people: {}", err);
//             return;
//         }
//     };

//     // Get person's name from command line argument
//     let args: Vec<String> = std::env::args().collect();
//     if args.len() != 2 {
//         eprintln!("Usage: {} <person_name>", args[0]);
//         return;
//     }
//     let person_name = &args[1];

//     // Find person by name and print information
//     match find_person_by_name(&people, person_name) {
//         Some(person) => println!("{:?}", person),
//         None => println!("Person not found"),
//     }
// }

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_person_by_name_existing() {
        let people = vec![
            Person {
                name: "John".to_string(),
                age: 73,
                location: "New York".to_string(),
            },
            Person {
                name: "Jane".to_string(),
                age: 99,
                location: "Los Angeles".to_string(),
            },
        ];

        assert_eq!(
            find_person_by_name(&people, "John"),
            Some(&Person {
                name: "John".to_string(),
                age: 73,
                location: "New York".to_string(),
            })
        );
    }

    #[test]
    fn test_find_person_by_name_not_existing() {
        let people = vec![
            Person {
                name: "John".to_string(),
                age: 73,
                location: "New York".to_string(),
            },
            Person {
                name: "Jane".to_string(),
                age: 99,
                location: "Los Angeles".to_string(),
            },
        ];

        assert_eq!(find_person_by_name(&people, "Doe"), None);
    }

    #[test]
    fn test_find_person_by_name_case_sensitive() {
        let people = vec![
            Person {
                name: "John".to_string(),
                age: 73,
                location: "New York".to_string(),
            },
            Person {
                name: "Jane".to_string(),
                age: 99,
                location: "Los Angeles".to_string(),
            },
        ];

        assert_eq!(
            find_person_by_name(&people, "john"),
            None
        );
    }
}
